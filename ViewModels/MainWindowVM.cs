﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Text;
using WpfYoubai.Models;

namespace WpfYoubai.ViewModels
{
    public class MainWindowVM
    {
        public Model _Model { get; } = new Model();

    }

    public class Model
    {
        public List<YoubaiRec> YoubaiRecs { get; set; } = new List<YoubaiRec>();
        
        public Model()
        {
            YoubaiRecs.Add(new YoubaiRec { Id = 1, FudeCode = "01001003", Kuiki = "2" });
            YoubaiRecs.Add(new YoubaiRec { Id = 2, FudeCode = "02001004", Kuiki = "4" });
            YoubaiRecs.Add(new YoubaiRec { Id = 3, FudeCode = "03001005", Kuiki = "1" });

        }
    }
}
