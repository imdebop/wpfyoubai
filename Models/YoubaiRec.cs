﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WpfYoubai.Models
{
    public class YoubaiRec
    {
        public int Id { get; set; }
        [DisplayName("筆コード")]
        public string FudeCode { get; set; }
        public string Kuiki { get; set; }
    }
}
